package postfixcalculator.mattiarubini.com.postfixcalculator.activity_file_directory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import postfixcalculator.mattiarubini.com.postfixcalculator.InputEquation;
import postfixcalculator.mattiarubini.com.postfixcalculator.PostfixCalculatorGlobalClass;
import postfixcalculator.mattiarubini.com.postfixcalculator.fragments.BannerAdsFragment;
import postfixcalculator.mattiarubini.com.postfixcalculator.R;

import static java.lang.Character.isDigit;

public class ActivityInfixToPostfix extends AppCompatActivity {

    //Needed to access the global class
    PostfixCalculatorGlobalClass mApp;

    //Object of the class containing the equation
    private InputEquation eq;

    /*InterstitialAd is a global variable needed to have interstitial ads in my app
    * The interstitial variable is global in because it has to be istantiate in the onCreate method and used in others method*/
    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infix_to_postfix);

        //with those 2 lines of code I get to display the ActionBar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.infix_to_postfix_toolbar);
        setSupportActionBar(myToolbar);
        /* With this code line I can change the title of the ActionBar
         * myToolbar.setTitle("Infix to Postfix");*/

        //Initializing my eq object
        eq = new InputEquation();

        //Initializating mApp
        mApp = ((PostfixCalculatorGlobalClass) getApplicationContext());

        //Initializing variable needed to have interstitial ads
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_id));
        //What happen when the ads close
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                printPostfix();
            }
        });
        //Must request an interstitial
        requestNewInterstitial();


        //Retriving preferences, to check if the item was bought
        boolean flagPurchase = this.getSharedPreferences(getString(R.string.preference_file_name),Context.MODE_PRIVATE).getBoolean(getString(R.string.purchase_status), false);
        Log.d("MainAct/onCreate","preference flag = "+flagPurchase);
        // Managing fragment
        // Check that the activity is using the layout version with the fragment_container FrameLayout
        if (findViewById(R.id.ads_banner) != null) {

            /* However, if we're being restored from a previous state,
             * then we don't need to do anything and should return or else
             * we could end up with overlapping fragments.*/
            if (savedInstanceState != null) {
                return;
            }

            if (flagPurchase == false){
                // Create a new Fragment to be placed in the activity layout
                BannerAdsFragment firstFragment = new BannerAdsFragment();

                /* In case this activity was started with special instructions from an
                 * Intent, pass the Intent's extras to the fragment as arguments*/
                firstFragment.setArguments(getIntent().getExtras());

                // Add the fragment to the 'fragment_container' FrameLayout
                getSupportFragmentManager().beginTransaction().add(R.id.ads_banner, firstFragment).commit();

            }

        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar,menu);
        //disattivo il tasto infixtopostfix nel dropdown menu
        menu.findItem(R.id.infixToPostfix).setEnabled(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.calculator:
                Intent intentToCalculator = new Intent(ActivityInfixToPostfix.this, MainActivity.class);
                startActivity(intentToCalculator);
                break;
            /**The OldInfixToPostfix option has been deactivated
            case R.id.infixToPostfix:
                Intent intentToInfixToPostfix = new Intent(ActivityStore.this, ActivityInfixToPostfix.class);
                startActivity(intentToInfixToPostfix);
                break; **/
             case R.id.store:
             Intent intentTostore = new Intent(ActivityInfixToPostfix.this, ActivityStore.class);
             startActivity(intentTostore);
             break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /*requestNewInterstitial creates an interstitial app
    * Method called only upon creation of the ad*/
    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                //This line has the sole purpose of creating a test ad
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    //Per stampare sullo schermo l'equazione
    private void printEquationScreen() {
        TextView txview = (TextView)findViewById(R.id.textEquation);
        //Se l'equazione è più lunga di 1 caratteri, allora c'è qualcosa da stampare, altrimenti non c'è niente da stampare
        if (eq.returnEquation() != "")
        {
            txview.setText(eq.returnEquation());
        }
        else
        {
            txview.setText("Equation");
        }
        Log.d("ActivityInfixToPostfix","Equation = "+eq.returnEquation()+ " full Equation = "+eq.returnFullEquation());
    }

    /* Manage wether:
     * 1) There suold be an interstitial ads or
     * 2) If it should lead directaly to the activity InfixToPostfixSteps*/
    public void printPostfixButton(View view) {
        //Increasing the number of uses the user did
        mApp.increaseUsesCount();
        if (mApp.retriveUsesCount() > 4) {
            if (mInterstitialAd.isLoaded()) {
                Log.d("sdgsfdaasdsdf","aasdadfdfad");
                //Display the interstitial ad
                mInterstitialAd.show();
            }
        } else {
            printPostfix();
        }
    }

    //Instantiate activity: InfixToPostfixSteps
    public void printPostfix () {
        Log.d("ActivityInfixToPostfix","Passing: Equation = "+eq.returnEquation()+" full Equation = "+eq.returnFullEquation());
        Intent intentToShowPostfixProcedure = new Intent(ActivityInfixToPostfix.this, ActivityInfixToPostfixSteps.class);
        intentToShowPostfixProcedure.putExtra("passingEquation",eq.returnFullEquation());
        startActivity(intentToShowPostfixProcedure);
    }

    //Add to the equation a number or a dot
    public void numbersButtons(View view) {
        char element = 0;
        switch (view.getId())
        {
            case R.id.button1:
                element = '1';
                break;

            case R.id.button2:
                element = '2';
                break;

            case R.id.button3:
                element = '3';
                break;

            case R.id.button4:
                element = '4';
                break;

            case R.id.button5:
                element = '5';
                break;

            case R.id.button6:
                element = '6';
                break;

            case R.id.button7:
                element = '7';
                break;

            case R.id.button8:
                element = '8';
                break;

            case R.id.button9:
                element = '9';
                break;

            case R.id.button0:
                element = '0';
                break;

            case R.id.buttonDot:
                element = '.';
                break;

            default:
                Log.d("ITPA/NumbersButton","Number Not Recognized");
                break;
        }
        if (element == '.'){
            eq.addDot();
        } else {
            eq.addNumber(element);
        }
        printEquationScreen();
    }

    //Add bracket to the equation
    public void bracketButtons(View view) {
        if (view.getTag().toString().charAt(0) == '(') {
            eq.openBracket();
        } else {
            eq.closeBracket();
        }
        printEquationScreen();
    }

    //Add an operator to the equation
    public void operatorsButtons(View view) {
        char element = '?';
        switch (view.getId())
        {
            /*case R.id.buttonEqual:
                DataProcessor.EnterEquation(equation, openBracket);
                PrintResultScreen(DataProcessor.SolveEquation());
                break;*/

            case R.id.buttonPlus:
                element = '+';
                break;

            case R.id.buttonMinus:
                element = '-';
                break;

            case R.id.buttonMult:
                element = '*';
                break;

            case R.id.buttonDiv:
                element = '/';
                break;

            case R.id.buttonPower:
                element = '^';
                break;

            default:
                Log.d("ITPA/OperatorButtons/sw","button not recognized");
                break;
        }
        eq.addOperator(element);
        printEquationScreen();
    }

    //Cancello un valore
    public void deleteButton(View view) {
        if (view.getTag().toString().equals("all")) {
            eq.deleteAll();
        } else {
            eq.deleteElement();
        }
        printEquationScreen();
    }
}