package postfixcalculator.mattiarubini.com.postfixcalculator;

/*InputEquation is a class that will hold the equation and will check the users input*/
public class InputEquation {

    private String equationString;
    private boolean flagNeutralElement;
    private boolean flagUsedDot;
    private Double neutralElement;
    private int openedBracket;

    /*The constructor will initialize all needed variable*/
    private void initVariable(){
        equationString = "0";
        flagNeutralElement = false;
        neutralElement = 0.0;
        flagUsedDot = false;
        openedBracket = 0;
    }

    public InputEquation (){
        initVariable();
    }

    /*This constructor exist for the only purpose of testing*/
    public InputEquation (String equation){
        equationString = '0' + equation;
        flagNeutralElement = false;
        flagUsedDot = false;
        neutralElement = 0.0;
    }

    /*In order to return a full equation, I need to close all the bracket*/
    public String closeEquationBracket (int n){
        String finalEquation = equationString;
        if (flagNeutralElement){
            if (neutralElement % 1 == 0){
                finalEquation += neutralElement.intValue();
            }else{
                finalEquation += neutralElement;
            }
        }
        if (finalEquation.charAt(finalEquation.length()-1) == '('){
            finalEquation += '0';
        }
        for (int i = 0; i < openedBracket - n; i++){
            finalEquation += ')';
        }
        return finalEquation;
    }

    /*returnEquation...*/
    public String returnEquation (){
        if (equationString.length() == 1){
            return "";
        } else {
            return equationString.substring(1, equationString.length());
        }
    }

    public String returnFullEquation () {
        return closeEquationBracket(0);
    }

    /*inputNumber is dedicated to adding number to the equation if possible*/
    public void addNumber (char digit){
        //This if is not needed anymore, after a ')' the InfixToPostfix class will add a *
        //if (equationString.length()-1 != ')'){
        //Aggiungere funzionalità elimino zero 05 -> 5
        if (flagNeutralElement == true) {
            flagNeutralElement = false;
        }
        equationString += digit;

    }

    /*addPoint only ads a point to the equation if possible*/
    public void addDot (){
    	/* must have either flagNeutralElement true or last character has to be a digit
    	 * must have flagUsedDot == false (the dot haven't been used yet)
    	 * (isDigit(lastChar) || flagNeutralElement) && !flagUsedDot*/
        //System.out.println("flagdot = "+flagUsedDot+" flagNeutral = "+flagNeutralElement);
        if ((Character.isDigit((equationString.charAt(equationString.length()-1))) || flagNeutralElement) && !flagUsedDot){
            //if the neutral element is 1, it has to be changed
            if (flagNeutralElement || equationString.length() < 2) {
                equationString += '0';
            }
            equationString += '.';
            flagUsedDot = true;
            flagNeutralElement = true;
            neutralElement = 0.0;
        }
    }

    //checks if we are dealing with an operator
    private boolean isOperator(char c) {
        boolean flag;
        if (c == '+' || c == '-' || c == '*' || c == '/' || c == '^') {
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    /*addOperator has the sole purpose of adding one of the following binary function: * + / - ^ */
    public void addOperator (char operator){
        char lastCharacter = equationString.charAt(equationString.length()-1);
        //First I check the specific case: equationString == "0-"
        if (equationString.length() == 2 &&  lastCharacter == '-'){
            deleteElement();
        }
        //I can't add an operator after an open bracket
        else if (lastCharacter != '('){
            //Special case: when I want to start the equation with a negative number -2 ----> 0-2
            if (equationString.length() == 1 && operator == '-'){
                equationString += '-';
                neutralElement = 0.0;
                flagNeutralElement = true;
            }
            //I can proceed normally when my equation string is > 1 in length
            else if (equationString.length() > 1) {
                //Check needed to not have an infinite number of minus
                if (lastCharacter == '-' && operator == '-'){
                    deleteElement();
                }
                //what do I do when I want to add an operator that is not a minus in a situation like 3*- (replace *- wit the new operator)
                if ((lastCharacter == '-') && (isOperator(equationString.charAt(equationString.length()-2))) && (operator != '-')){
                    deleteElement();
                    deleteElement();
                    lastCharacter = equationString.charAt(equationString.length()-1);
                }
    			/* If the last character is an operator and the selected operator is NOT a minus, delete the old operator
    			 * Limit case (- */
                if (isOperator(lastCharacter) && (equationString.charAt(equationString.length()-2) != '(') && (operator != '-')){
                    deleteElement();
                }
                //If I'm in case such as *- I can't allow more than 1 - (3*-2 ok / 3*----2 nope)
	    		/*If there is a neutral element it has to be added before the operator
	    		 * Except when the neutral element is from an operator*/
                if (flagNeutralElement == true && equationString.length() > 1 && !isOperator(lastCharacter)){
                    equationString += neutralElement;
                    flagNeutralElement = false;
                }
	    		/* This check is triky
	    		 * First condition is to avoid cases such as (4-
	    		 * Second condition is to avoid cases such as -(-
	    		 * */
                if (Character.isDigit(equationString.charAt(equationString.length()-1)) || equationString.charAt(equationString.length()-2) != '(') {
                    //Adding the operator
                    equationString += operator;
                    //Setting the right neutral element in case of *-
                    if (equationString.charAt(equationString.length()-1) == '-' && isOperator(equationString.charAt(equationString.length()-2))){
                        neutralElement = operatorNeutralElement(equationString.charAt(equationString.length()-2));
                    } else {
                        neutralElement = operatorNeutralElement(operator);
                    }
                    flagNeutralElement = true;
                    flagUsedDot = false;
                }
            }
        }
        //The only operator that can be added after a an open bracket is a minus
        else if (operator == '-') {
            equationString += "-";
            flagNeutralElement = true;
            neutralElement = 0.0;
        }
    }

    public void openBracket (){
        //I have to modify the string in case the first character is a '('
        if (equationString.length() == 1){
            equationString = "((";
            openedBracket = 2;
            flagNeutralElement = false;
            flagUsedDot = true;
        } else {
            char lastCharacter = equationString.charAt(equationString.length()-1);
            //I can't open a bracket after a '.'
            if (lastCharacter == '.'){
                //I have to erase the '.'
                deleteElement();
                equationString += '*';
                //After a number or a closed bracket, I'll have to add a '*' to the
            }
            //This part is no longer needed since It's managed in the InfixToPostfix class in the prepareEquation function
	    	/*else if (Character.isDigit(lastCharacter) || lastCharacter == ')'){
	    		addOperator('*');
	    	}*/
            //Only case left is after an operator, but I don't have to modify anything
            equationString += '(';
            flagUsedDot = true;
            flagNeutralElement = false;
            openedBracket += 1;
        }
    }

    public void closeBracket(){
        char lastCharacter = equationString.charAt(equationString.length()-1);
        //There must be open bracket
        if (openedBracket > 0 && lastCharacter != '('){
            if (isOperator(lastCharacter)){
                deleteElement();
            }
            if (flagNeutralElement){
                flagNeutralElement = false;
                equationString += neutralElement;
            }
            equationString += ')';
            flagUsedDot = false;
            openedBracket -= 1;
        }
    }

    //give back the chosen operator neutral element
    public Double operatorNeutralElement (char operator){
        if (operator == '+' || operator == '-') {
            return 0.0;
        } else {
            return 1.0;
        }
    }

    /*deleteElement delete the last element of the equation*/
    public void deleteElement (){
        if (equationString.length() > 2){
            char finalElement = equationString.charAt(equationString.length()-1);
            //Number of open bracket reduced
            if (finalElement == '('){
                openedBracket--;
            }
            //Erasing a closing bracket increase the number of '(' left un-closed
            else if (finalElement == ')'){
                openedBracket++;
            }
            //if the last element of the equation is a digit, I have to deactivate the neutral element
            if (Character.isDigit(finalElement)){
                flagNeutralElement = false;
            }
            //when erasing a dot reactivate the flagDot
            if (finalElement == ',') {
                flagUsedDot = false;
            }
            //When erasing an operator I have to deactivate the neutral element
            if (isOperator(finalElement)){
                flagNeutralElement = false;
            }
            equationString = equationString.substring(0,equationString.length()-1);
            //If the last element of the new equation is an operator: restore the neutral element
            if (isOperator(equationString.charAt(equationString.length()-1))){
                flagNeutralElement = true;
                if (equationString.length() > 2 && isOperator(equationString.charAt(equationString.length()-2))) {
                    neutralElement = operatorNeutralElement(equationString.charAt(equationString.length()-2));
                } else {
                    neutralElement = operatorNeutralElement(equationString.charAt(equationString.length()-1));
                }
            }
        } else {
            initVariable();
        }
    }

    //Restore the eq to it's original state
    public void deleteAll (){
        initVariable();
    }
}