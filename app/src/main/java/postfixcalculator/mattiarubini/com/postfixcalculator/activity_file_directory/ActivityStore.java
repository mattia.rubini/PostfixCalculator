package postfixcalculator.mattiarubini.com.postfixcalculator.activity_file_directory;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;

import java.util.ArrayList;

import postfixcalculator.mattiarubini.com.postfixcalculator.PostfixCalculatorGlobalClass;
import postfixcalculator.mattiarubini.com.postfixcalculator.fragments.BannerAdsFragment;
import postfixcalculator.mattiarubini.com.postfixcalculator.R;


public class ActivityStore extends AppCompatActivity {

    /*My variable related to the connection
     *Those variable is required to communicate with the play sotre*/
    IInAppBillingService mService;
    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            //The connection has been closed
            flagConnctionEstablished = false;
            ScreenPrint("Disconnected");
            Log.d("Store/onServiceDisconec","The connection is closed");
        }
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            //The connection has been opened
            flagConnctionEstablished = true;
            ScreenPrint("Connected");
            Log.d("Store/onServiceConnect","The connection is open");
        }
    };

    //Needed to access the global class
    PostfixCalculatorGlobalClass mApp;

    //Flag that indicate the status of the connection (true = online, false = offline)
    boolean flagConnctionEstablished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Loading the XML intefarce
        setContentView(R.layout.activity_store);
        //Loading the toolbar interface
        Toolbar myToolbar = (Toolbar) findViewById(R.id.infix_to_postfix_toolbar);
        setSupportActionBar(myToolbar);

        //Initializating mApp
        mApp = ((PostfixCalculatorGlobalClass) getApplicationContext());

        //Opening a connection with the store
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        //Retriving preferences, to check if the item was bought
        //boolean flagPurchase = this.getSharedPreferences(getString(R.string.preference_file_name),Context.MODE_PRIVATE).getBoolean(getString(R.string.purchase_status), false);
        boolean flagPurchase = mApp.retrievePreferences();
        Log.d("MainAct/onCreate","preference flag = "+flagPurchase);
        // Managing fragment
        // Check that the activity is using the layout version with the fragment_container FrameLayout
        if (findViewById(R.id.ads_banner) != null) {

            /* However, if we're being restored from a previous state,
             * then we don't need to do anything and should return or else
             * we could end up with overlapping fragments.*/
            if (savedInstanceState != null) {
                return;
            }

            if (flagPurchase == false){
                // Create a new Fragment to be placed in the activity layout
                BannerAdsFragment firstFragment = new BannerAdsFragment();

                /* In case this activity was started with special instructions from an
                 * Intent, pass the Intent's extras to the fragment as arguments*/
                firstFragment.setArguments(getIntent().getExtras());

                // Add the fragment to the 'fragment_container' FrameLayout
                getSupportFragmentManager().beginTransaction().add(R.id.ads_banner, firstFragment).commit();

            }

        }
    }

    //The purpose of this method is to customize the AtionBar
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar,menu);
        //I disable the calculator option in the dropdown menù in the action bar
        menu.findItem(R.id.store).setEnabled(false);
        return true;
    }

    //Handle actionbar menu item selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.calculator:
                Intent intentToCalculator = new Intent(ActivityStore.this, MainActivity.class);
                startActivity(intentToCalculator);
                break;
            case R.id.infixToPostfix:
                Intent intentToInfixToPostfix = new Intent(ActivityStore.this, ActivityInfixToPostfix.class);
                startActivity(intentToInfixToPostfix);
                break;
            /**The store option has been deactivated
            case R.id.store:
                Intent intentTostore = new Intent(ActivityStore.this, ActivityStore.class);
                startActivity(intentTostore);
                break;**/
            /*case R.id.action_settings:
                break;*/
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Always unbind the connection with the store, otherwise performance degradation of the device may follow.
        if (mService != null) {
            unbindService(mServiceConn);
        }

    }

    //Method that manage the Store buttons
    public void StoreButtons (View view){
        switch (view.getId()){
            case R.id.purchase:
                if (flagConnctionEstablished) {
                    PurchaseAction();
                }else{
                    ScreenPrint("wait, establishing connection");
                }
                break;
            case R.id.restore:
                if (flagConnctionEstablished) {
                    PurchaseCheck();
                }else{
                    ScreenPrint("wait, establishing connection");
                }
                break;
            /*case R.id.welcome:
                ScreenPrint("Welcome");
                break;*/
            default:
                Log.d("Store/StoreButtons","Wrong button?");
                break;
        }
    }

    //Method that process the purchases
    public void PurchaseAction (){
        //try is needed in order to
        try {
            ScreenPrint("Initiating request for purchase");
            Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(), "full_unlock_key", "inapp", "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
            ScreenPrint("Connection established");
            //Check if billing is working ok
            if (buyIntentBundle.getInt("BILLING_RESPONSE_RESULT_OK") == 0) {
                Log.d("Store/PurchaseAction","Billing was successful");
                PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                startIntentSenderForResult(pendingIntent.getIntentSender(), 1001, new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));
            } else {
                //in this case something went wrong
                Log.d("","Something went wrong with the purchase");
            }
        }
        //Needed to catch the RemoteException or the buyIntentBundle wont work
        catch (RemoteException e){
            Log.d("Store/PurchaseAction","Couldn't buy the item, RemoteException:",e);
        }
        //Needed to catch an Exception from startIntentSenderForResult
        catch (Exception e){
            Log.d("Store/PurchaseAction","Couldn't buy the item, exception:",e);
        }
        if (PurchaseCheck()) {
            ScreenPrint("The purchase was successful");
        } else {
            ScreenPrint("The purchase wasn't successful, try again later or the restore purchase button");
        }
    }

    //Method used for restoring the purchases
    public boolean PurchaseCheck(){
        //I'm returning a boolean to know if everything was alright
        boolean flag;
        //Since I need to initialize flag, I will initialize flag with the value stored in the saved preferences
        flag = mApp.retrievePreferences();
        try {
            Log.d("StoreAct/PurchaseCheck", "Asking for data");
            //Asking for data
            Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
            Log.d("StoreAct/PurchaseCheck", "Data recieved");
            //Checking data
            int response = ownedItems.getInt("RESPONSE_CODE");
            if (response == 0) {
                //Extracting data
                Log.d("StoreAct/PurchaseCheck", "Data recievec successfully");
                ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String> purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                //Check if something was bought
                if (purchaseDataList.size() > 0) {
                    String s = ownedSkus.get(purchaseDataList.size() - 1);
                    Log.d("StoreAct/PurchaseCheck", "The item that was bought is: " + s);
                    ScreenPrint("Your order was restored, restar the app if the ads remain");
                    flag = true;
                } else {
                    Log.d("StoreAct/PurchaseCheck", "The user didn't bought anything");
                    flag = false;
                    ScreenPrint("There is no purchase related to your account");
                }
            } else {
                Log.d("StoreAct/PurchaseCheck", "There are no items to buy");
            }
        } catch (Exception e) {
            //Something went wrong when checking if the purchase ad already been made
            Log.d("StoreAct/PurchaseCheck", "Someting went wrong with the check, error: ", e);
        }
        //I'm updating the preference file based on the purchase
        mApp.changePreference(flag);
        return flag;
    }

    //Method that prints o the lower text in the xml
    public void ScreenPrint (String s){
        TextView txview = (TextView)findViewById(R.id.storeText);
        txview.setText(s);
    }
}
