package postfixcalculator.mattiarubini.com.postfixcalculator;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class PostfixCalculatorGlobalClass extends Application {

    /* numUses indicates how many time some features have been used.
     * Every 5 uses an interstitial ad is showened
     * It's value can ONLY be a number between 0 and 5*/
    static private int numUses;

    /* The contructor of PostfixCalculatorGlobalClass only initialize numUses*/
    public PostfixCalculatorGlobalClass (){
        numUses = 0;
    }

    /*retriveUsesCount returns the value of numUses*/
    public int retriveUsesCount(){
        return numUses;
    }

    /*increaseUsesCount increase the value of numUses of 1
    * When num uses is equal to 5 and this method is called, numUses is set to 1*/
    public void increaseUsesCount() {
        if (numUses == 5){
            numUses = 1;
        } else {
            numUses = numUses + 1;
        }
    }

    public void resetUsesCount (){
        numUses = 0;
    }

    public void changePreference(boolean flag){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.purchase_status), flag);
        editor.apply();
    }

    public boolean retrievePreferences(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_name), Context.MODE_PRIVATE);
        /*If the file doesn't exist, I create the file and I'm returned with a false value.
        * Because if the file was not created it's likely to be the first install.
        * If the user acquired the product on the store, it will be able to restore its purchase.*/
        return (sharedPref.getBoolean(getString(R.string.purchase_status), false));
    }
}
