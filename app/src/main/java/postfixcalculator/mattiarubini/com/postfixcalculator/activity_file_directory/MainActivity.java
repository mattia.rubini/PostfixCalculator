package postfixcalculator.mattiarubini.com.postfixcalculator.activity_file_directory;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.os.IBinder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;

import java.util.ArrayList;

import postfixcalculator.mattiarubini.com.postfixcalculator.InputEquation;
import postfixcalculator.mattiarubini.com.postfixcalculator.PostfixCalculatorGlobalClass;
import postfixcalculator.mattiarubini.com.postfixcalculator.fragments.BannerAdsFragment;
import postfixcalculator.mattiarubini.com.postfixcalculator.R;
import postfixcalculator.mattiarubini.com.postfixcalculator.infix_to_postfix_classes.InfixToPostfix;

public class MainActivity extends AppCompatActivity {

    //Object of the class containing the equation
    private InputEquation eq;
    private InfixToPostfix eqPostfix;

    //Needed to access the global class
    //PostfixCalculatorGlobalClass mApp = ((PostfixCalculatorGlobalClass) getApplicationContext());
    PostfixCalculatorGlobalClass mApp;

    //Those variable is required to communicate with the play sotre
    IInAppBillingService mService;
    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            Log.d("MainAct/onServiceDiscon","The connection is closed");
        }
        /*Very usefull class
        * All the contenet start only AFTER THE CONNECTION IS SET
        * Used to perform check on the connection*/
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            //I added this line of code to make sure that the check was made only after the connection was established
            CheckPurchase();
            Log.d("MainAct/onServiceConnec","The connection is open");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Indicate wich layout to use for this activity
        setContentView(R.layout.activity_main);
        //used to personalize this activity toolbar on the top
        Toolbar myToolbar = (Toolbar) findViewById(R.id.calculator_toobar);
        setSupportActionBar(myToolbar);

        //Initialization of the eq
        eq = new InputEquation();

        //Opening a connection with the store
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        //Initializating mApp
        mApp = ((PostfixCalculatorGlobalClass) getApplicationContext());
        //Setting to 0 the numUses is fundamental for the wole project
        mApp.resetUsesCount();

        //Retriving preferences, to check if the item was bought
        //boolean flagPurchase = this.getSharedPreferences(getString(R.string.preference_file_name),Context.MODE_PRIVATE).getBoolean(getString(R.string.purchase_status), false);
        boolean flagPurchase = mApp.retrievePreferences();
        Log.d("MainAct/onCreate","preference flag = "+flagPurchase);
        // Managing fragment
        // Check that the activity is using the layout version with the fragment_container FrameLayout
        if (findViewById(R.id.ads_banner) != null) {

            /* However, if we're being restored from a previous state,
             * then we don't need to do anything and should return or else
             * we could end up with overlapping fragments.*/
            if (savedInstanceState != null) {
                return;
            }

            if (flagPurchase == false){
                // Create a new Fragment to be placed in the activity layout
                BannerAdsFragment firstFragment = new BannerAdsFragment();
                /* In case this activity was started with special instructions from an
                 * Intent, pass the Intent's extras to the fragment as arguments*/
                firstFragment.setArguments(getIntent().getExtras());

                // Add the fragment to the 'fragment_container' FrameLayout
                getSupportFragmentManager().beginTransaction().add(R.id.ads_banner, firstFragment).commit();

            }
        }
    }

    //The purpose of this method is to customize the AtionBar
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar,menu);
        //I disable the calculator option in the dropdown menù in the action bar
        menu.findItem(R.id.calculator).setEnabled(false);
        return true;
    }

    //This method manages the drop down menù selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            /**The Calculator option has been deactivated
            case R.id.calculator:
                Intent intentToCalculator = new Intent(ActivityStore.this, MainActivity.class);
                startActivity(intentToCalculator);
                break; **/
            case R.id.infixToPostfix:
                Intent intentToInfixToPostfix = new Intent(MainActivity.this, ActivityInfixToPostfix.class);
                startActivity(intentToInfixToPostfix);
                break;

             case R.id.store:
             Intent intentTostore = new Intent(MainActivity.this, ActivityStore.class);
             startActivity(intentTostore);
             break;
            /*case R.id.action_settings:
                break;*/
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Always unbind the connection with the store, otherwise performance degradation of the device may follow.
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }

    //Method called to check if the unlock was bought or not
    public boolean CheckPurchase (){
        //I'm returning a boolean to know if everything was alright
        boolean flag;
        //Since I need to initialize flag, I will initialize flag with the value stored in the saved preferences
        flag = mApp.retrievePreferences();
        try {
            Log.d("MainAct/RestorePurchAct", "Asking for data");
            //Asking for data
            Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
            Log.d("MainAct/RestorePurchAct", "Data recieved");
            //Checking data
            int response = ownedItems.getInt("RESPONSE_CODE");
            if (response == 0) {
                //Extracting data
                Log.d("Store/RestorePurchAct", "Data recievec successfully");
                ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String> purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                //Check if something was bought
                if (purchaseDataList.size() > 0) {
                    String s = ownedSkus.get(purchaseDataList.size() - 1);
                    Log.d("MainAct/CheckPurchase", "The item that was bought is: " + s);
                    flag = true;
                } else {
                    Log.d("MainAct/CheckPurchase", "you didn't buy anything");
                    flag = false;
                }
            } else {
                Log.d("MainAct/CheckPurchase", "There are no items to buy");
            }
            //I've added this line of code because is needed to the while try catch loop
        } catch (Exception e) {
            Log.d("MainAct/RestorePurchAct", "Someting went wrong with the check, error: ", e);
        }
        //Always unbind the connection with the store, otherwise performance degradation of the device may follow.
        if (mService != null) {
            unbindService(mServiceConn);
        }
        //I'm updating the preference file based on the purchase
        mApp.changePreference(flag);
        return flag;
    }

    //metodo per stampare l'equazione nell'appositto quadrante
    private void printEquationScreen() {
        TextView txview = (TextView)findViewById(R.id.textEquation);
        if (eq.returnEquation() != ""){
            txview.setText(eq.returnEquation());
        } else {
            txview.setText("Equation");
        }
        Log.d("MA/PrintEquationScreen ","equation = '"+eq.returnEquation()+"' Full Equation = '"+eq.returnFullEquation()+"'");
        PrintResultScreen();
    }

    //metodo per stampare il risultato dell'equazione nell'apposito quadrante
    private void PrintResultScreen () {
        TextView txview = (TextView)findViewById(R.id.textResult);
        if (eq.returnEquation() == "") {
            txview.setText("Results");
        } else {
            try{
                eqPostfix = new InfixToPostfix(eq.returnFullEquation());
                Log.d("MainActivity","PrintResultScreen: postfix quation = "+eqPostfix.returnPreWorkEquation());
                Log.d("MainActivity","PrintResultScreen: postfix quation = "+eqPostfix.returnPostfixEquation());
                txview.setText(Double.toString(eqPostfix.returnResult()));
            } catch (Exception e){
                Log.d("MainActivity","Erro while calculating the result: erorr = "+e);
                txview.setText("Wrong Equation Syntax");
            }
        }
    }

    //Add to the equation both dot and digit
    public void numbersButtons(View view) {
        char element = 0;
        switch (view.getId()) {
            case R.id.button1:
                element = '1';
                break;

            case R.id.button2:
                element = '2';
                break;

            case R.id.button3:
                element = '3';
                break;

            case R.id.button4:
                element = '4';
                break;

            case R.id.button5:
                element = '5';
                break;

            case R.id.button6:
                element = '6';
                break;

            case R.id.button7:
                element = '7';
                break;

            case R.id.button8:
                element = '8';
                break;

            case R.id.button9:
                element = '9';
                break;

            case R.id.button0:
                element = '0';
                break;

            case R.id.buttonDot:
                element = '.';
                break;

            default:
                Log.d("MA/NumbersButton", "Number Not Recognized");
                break;
        }
        if (element == '.'){
            eq.addDot();
        } else {
            eq.addNumber(element);
        }
        printEquationScreen();
    }

    //Add bracket to the equation
    public void bracketButtons(View view) {
        if (view.getTag().toString().charAt(0) == '('){
            eq.openBracket();
        } else {
            eq.closeBracket();
        }
        printEquationScreen();
    }

    //Add an operator to the equation
    public void operatorsButtons(View view) {
        eq.addOperator(view.getTag().toString().charAt(0));
        printEquationScreen();
    }

    //Erase a button or all the equation
    public void deleteButton(View view) {
        if (view.getTag().toString().equals("all")) {
            eq.deleteAll();
        } else {
            eq.deleteElement();
        }
        printEquationScreen();
    }
}