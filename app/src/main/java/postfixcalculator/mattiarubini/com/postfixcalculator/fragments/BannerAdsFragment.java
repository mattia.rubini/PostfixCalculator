package postfixcalculator.mattiarubini.com.postfixcalculator.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import postfixcalculator.mattiarubini.com.postfixcalculator.R;

public class BannerAdsFragment extends Fragment {

    private static final String TAG = "MainActivity";
    private AdView mAdView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.banner_ad_on_top, null);
        mAdView = (AdView) rootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // Inflate the layout for this fragment
        return rootView;
    }
}
