package postfixcalculator.mattiarubini.com.postfixcalculator.infix_to_postfix_classes;

import android.util.Log;

import java.util.List;
import java.util.Stack;
import java.util.ArrayList;

public class InfixToPostfix {

    private List <Double> listOfValue;
    private List <String> listOfSteps;
    private String originalEquation;
    private String preWorkEquation;
    private String postfixEquation;
    private String realPostfixEquation;
    private double result;

    //Constructor
    public InfixToPostfix(String equation){
        originalEquation = equation;
        preWorkEquation = "";
        postfixEquation = "";
        realPostfixEquation = "";
        listOfValue = new ArrayList <>();
        listOfSteps = new ArrayList <>();
        listOfSteps.add("The text is scrollable \n\n");
        result = 0;
        prepareEquation();
        //I'm using \n when I'm printing a String and <br /> when printing in HTML
        if (originalEquation.charAt(0) == '0'){
            listOfSteps.add("The Infix equation is: <font color=#00cc00>"+originalEquation.substring(1, originalEquation.length())+"</font><br />");
        } else {
            listOfSteps.add("The Infix equation is: <font color=#00cc00>"+originalEquation+"</font><br />");
        }
        infixToPostfix();
        realPostfixEquation = postfixEquationInsertRealValue();
        listOfSteps.add(2,"The Postfix equation is: <font color=#0000ff>"+realPostfixEquation+"</font><br /> <br /> ");
    }

    public String returnPreWorkEquation (){
        return preWorkEquation;
    }

    public String returnPostfixEquation (){
        return realPostfixEquation;
    }

    public double returnResult () {
        calculateResult();
        return result;
    }

    public List <String> returnSteps (){
        return listOfSteps;
    }

    //The name said it all
    private boolean isOperator(char c) {
        return (c == '+' || c == '-' || c == '*' || c == '/' || c == '^');
    }

    //The equation need to be modified in order to be 100% compatible with this clas functions
    public void prepareEquation (){
        boolean flagNegativeNumber = false;
        int start = 0;
        //Special Case: the equation starts with a -2 full equation = 0-2
        if (originalEquation.charAt(1) == '-'){
            start = 2;
            flagNegativeNumber = true;
        }
        for (int i = start; i < originalEquation.length(); i++){
            //First scenario: bracket (open and close)
            if (originalEquation.charAt(i) == '(' || originalEquation.charAt(i) == ')'){
				/* Adding the * between a digit and an open bracket
				 * The first two check are mandatory for null reference*/
                if ((i > 0) && preWorkEquation.length() > 0 && originalEquation.charAt(i) == '(' && preWorkEquation.charAt(preWorkEquation.length()-1) == ','){
                    preWorkEquation += '*';
                }
                preWorkEquation += originalEquation.charAt(i);
            }
            //Second scenario: operator
            else if (isOperator(originalEquation.charAt(i))){
                //If I'm dealing with a minus after an operator the next number will be negative Ex: *-3
                if (originalEquation.charAt(i-1) == '(' || isOperator(originalEquation.charAt(i-1)) && originalEquation.charAt(i) == '-') {
                    //The next number will be a negative one
                    flagNegativeNumber = true;
                } else {
                    //if it is only an operator, I need to write it down
                    preWorkEquation += originalEquation.charAt(i);
                }

            }
            //Third scenario: number
            else if ((Character.isDigit(originalEquation.charAt(i)) /*|| originalEquation.charAt(i) == '.'*/)){
				/* In case I get a closed bracket followed by a digit, I have to add in between a '*'
				 * i > 0 is needed to avoid to check the 0-1 element of the string in case the equation starts with a number*/
                if (i > 0 && originalEquation.charAt(i-1) == ')'){
                    preWorkEquation += '*';
                }
                //num will be my number
                double num = 0;
                int postDot = 0;
                boolean flagDot = false;
				/* (i < originalEquation.length()) is needed in the case the equation ends with a number
				 * Ex: 3*(4-2)/4
				 * If the equation ends with a number the charAt(i) in the while condition will cause problems*/
                while ((i < originalEquation.length()) && (Character.isDigit(originalEquation.charAt(i)) || (originalEquation.charAt(i) == '.'))) {
                    //activate the count that follow the dot
                    if ((originalEquation.charAt(i) == '.')) {
                        flagDot = true;
                        //if it's a digit
                    } else {
                        //I'll divide by ten for every digit after the dot (postDot)
                        if (flagDot == true) {
                            postDot -= 1;
                        }
                        num *= 10;
                        num += Character.getNumericValue(originalEquation.charAt(i));
                    }
                    i++;
                }
                //Needed to "put the dot" in the number
                num *= Math.pow(10, postDot);
                //checking if the number has to be a negative one
                if (flagNegativeNumber){
                    num *= -1;
                    flagNegativeNumber = false;
                }
                //adding the number to the list
                listOfValue.add(num);
                //inserting the number in the list
                preWorkEquation += listOfValue.size()-1;
                //adding a comma for later use
                preWorkEquation += ',';
                //It's needed to not loose character
                i--;
            }
            //Print needed to check step by step
            //System.out.println("n"+i+" = "+preWorkEquation);
            //System.out.println(listOfValue);
        }
    }

    //Manage priority between operators (including the open and closed brackets)
    private int operatorPriority(Character operator1){
        switch(operator1)
        {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            case '^':
                return 3;
            case '(':
                return 4;
            default:
                return 0;
        }
    }

    //Add a step to the list of steps that shows how the postfix magic is done
    private void addStepToList (String element, Stack <Character> stck){
        int num= ((listOfSteps.size()-2)/4)+1;
        listOfSteps.add("<font color=#ff0000>Step "+num+"</font><br />");
        if (!Character.isDigit(element.charAt(0))){
            listOfSteps.add("working on element: <font color=#ff33cc>"+element+"<br />");
        } else{
            if (listOfValue.get(Integer.parseInt(element)) % 1 == 0){
                listOfSteps.add("working on element: <font color=#ff33cc>"+listOfValue.get(Integer.parseInt(element)).intValue()+"</font><br />");
            }else{
                listOfSteps.add("working on element: <font color=#ff33cc>"+listOfValue.get(Integer.parseInt(element))+"</font><br />");
            }
        }
        listOfSteps.add("stack: <font color=#ffcc00>"+stck+"</font><br />");
        String postfixEq = postfixEquationInsertRealValue();
        listOfSteps.add("postfix equation: <font color=#0000ff>"+postfixEq+"</font><br /><br />");
    }

    //Convert the preWorkEquation in postfixEquation (infix ---> postfix)
    private void infixToPostfix (){
        //This flag is used when adding stuff to the stack (used for printing steps)
        boolean flagAddedToStack = false;
        Stack <Character> operatorStack = new Stack<Character>();;
        for (int i = 0 ; i < preWorkEquation.length(); i++){
            //System.out.println("i = "+i);
            //In case of a digit or a ','
            if (Character.isDigit(preWorkEquation.charAt(i)) || preWorkEquation.charAt(i) == ','){
                postfixEquation += preWorkEquation.charAt(i);
            }
            //In case I deal with an operator or a open bracket
            else if (isOperator(preWorkEquation.charAt(i)) || preWorkEquation.charAt(i) == '('){
                flagAddedToStack = true;
                //Activate when: (1) the stack is not empty (2) on top of the stack there isn't a '(' (3) the operator on top of the stack higher or equal priority
                while ((!operatorStack.isEmpty()) && (operatorStack.peek() != '(') && (operatorPriority(operatorStack.peek()) >= operatorPriority(preWorkEquation.charAt(i))) ){
                    postfixEquation += operatorStack.pop();
                }
                //I need to push the operator or the open bracket, no matter what
                operatorStack.push(preWorkEquation.charAt(i));
            }
            //In the case I get a closed bracket
            else if (preWorkEquation.charAt(i) == ')') {
                flagAddedToStack = true;
                //I'll repeat the loop until: (1) there is something in the stack (2) the the top of the stack isn't an open bracket
                while ((!operatorStack.isEmpty()) && (operatorStack.peek() != '(')){
                    postfixEquation += operatorStack.pop() ;
                }
                //If there is a open bracket on the top of the stack, it has to be eliminated
                if (!operatorStack.isEmpty()){
                    operatorStack.pop();
                }
            }
            //System.out.println(postfixEquation);
            //From here on we work on the step by step print on the screen
            if (postfixEquation.length() > 0) {
                //If I added something to the stack I'll do that
                if (flagAddedToStack == true){
                    if (preWorkEquation.charAt(i) == ')') {
                        addStepToList(")", operatorStack);
                    } else{
                        addStepToList(Character.toString(operatorStack.peek()), operatorStack);
                    }
                    flagAddedToStack = false;
                }
                //Else if there are commas I need to get the number there was before the comma
                else if (postfixEquation.charAt(postfixEquation.length()-1) == ',') {
                    int e = postfixEquation.length()-1;
                    //first I need to know were the number start
                    while ((e-1 >= 0) && (Character.isDigit(postfixEquation.charAt(e-1))) ){
                        e--;
                    }
                    //I'll recover the number as a string
                    String number = "";
                    for (int j = e; j < postfixEquation.length()-1; j++){
                        number += postfixEquation.charAt(j);
                    }
                    //Now that I got the number, wich is just an index, I'll have to add it
                    addStepToList(number, operatorStack);
                }
            }
        }
        if (!operatorStack.isEmpty()){
            listOfSteps.add("<font color=#ff0000>Finally:</font><br />All the equation has been parsed,but the stack isn't empty: therefore it has to be unloaded"+"<br />"+"Stack: <font color=#ffcc00>"+operatorStack+"</font><br />");
            //Empty the stack if it's not empty
            while(!operatorStack.isEmpty()){
                postfixEquation += operatorStack.pop();
            }
            listOfSteps.add("Postfix equation: <font color=#0000ff>" + postfixEquationInsertRealValue() + "<font><br />");
        }

    }

    //Restore the Postfix string with the proper numeric value and not the respective index
    private String postfixEquationInsertRealValue () {
        String number = "";
        String postfixEquationWithValue = "";
        for (int i = 0; i < postfixEquation.length(); i++){
            //If I find a comma I have to reconstruct the number
            if (postfixEquation.charAt(i) == ',') {
                int e = i;
                //first I need to know were the number start
                while ((e-1 >= 0) && (Character.isDigit(postfixEquation.charAt(e-1))) ){
                    e--;
                }
                //I'll recover the number as a string
                for (int j = e; j < i; j++){
                    number += postfixEquation.charAt(j);
                }
                //Now that I got the number, wich is just an index, I'll have to add it
                Double realNumber = listOfValue.get(Integer.parseInt(number));
                //if the number can be divided by 1 it can be aproxymated to an integer
                if (realNumber % 1 == 0){
                    postfixEquationWithValue += Integer.toString(realNumber.intValue()) + " ";
                } else {
                    postfixEquationWithValue += Double.toString(realNumber) + " ";
                }
                number = "";
            } else if (!Character.isDigit(postfixEquation.charAt(i))){
                postfixEquationWithValue += postfixEquation.charAt(i) + " ";
            }
        }
        return postfixEquationWithValue;
    }

    //Simply calculate the numeric value
    private void calculateResult (){
        //First I need to cycle all the postfix equation
        Stack <Double> numStack = new Stack <>();
        for (int i = 0; i < postfixEquation.length(); i++) {
            //Case 1: encountering a number
            if (Character.isDigit(postfixEquation.charAt(i))){
                int num = Character.getNumericValue(postfixEquation.charAt(i));
                while (postfixEquation.charAt(i+1) != ',') {
                    i++;
                    num *= 10;
                    num += Character.getNumericValue(postfixEquation.charAt(i));
                }
                i++;
                numStack.push(listOfValue.get(num));
            } else {
                //Need to initialize those two variable because 1/2 is different than 2/1
                double d1 = numStack.pop();
                double d2;
                switch (postfixEquation.charAt(i)){
                    case '+':
                        d2 = numStack.pop();
                        numStack.push(d2 + d1);
                        break;
                    case '-':
                        d2 = numStack.pop();
                        numStack.push(d2 - d1);
                        break;
                    case '*':
                        d2 = numStack.pop();
                        numStack.push(d2 * d1);
                        break;
                    case '/':
                        d2 = numStack.pop();
                        numStack.push(d2 / d1);
                        break;
                    case '^':
                        d2 = numStack.pop();
                        numStack.push(Math.pow(d2, d1));
                        break;
                    default:
                        System.out.println("operation "+postfixEquation.charAt(i)+" not found");
                        break;
                }
            }
            //System.out.println(numStack);
        }
        result = numStack.pop();
    }
}