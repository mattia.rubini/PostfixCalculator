package postfixcalculator.mattiarubini.com.postfixcalculator.activity_file_directory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import postfixcalculator.mattiarubini.com.postfixcalculator.fragments.BannerAdsFragment;
import postfixcalculator.mattiarubini.com.postfixcalculator.R;
import postfixcalculator.mattiarubini.com.postfixcalculator.infix_to_postfix_classes.InfixToPostfix;

import static java.lang.Character.isDigit;

public class ActivityInfixToPostfixSteps extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infix_to_postfix_steps);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.infix_to_postfix_toolbar_procedure);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Getting a hold on the text view
        TextView txview = (TextView)findViewById(R.id.postfix_jurney_text_wall);

        //Recovering data from the father activity (ActivityInfixToPostfix)
        String equation = getIntent().getStringExtra("passingEquation");

        Log.d("InfixToPostfixSteps","passed eqaution = "+equation);

        try {
            //Creating an object containing all the steps
            InfixToPostfix eq = new InfixToPostfix(equation);
            //Recovering the steps
            List<String> stepsList = new ArrayList<>(eq.returnSteps());

            /* Spanned is needed to use HTML tags in the text
             * Those tags are needed to see only part of some line being colored*/
            Spanned htmlContenet;

            //Printing All the Steps
            txview.setText(stepsList.get(0));
            for (int i = 1; i < stepsList.size(); i++) {
                htmlContenet = Html.fromHtml(stepsList.get(i));
                txview.append(htmlContenet);
            }
        } catch (Exception e){
            txview.setText("Syntax error with the equation");
            Log.d("ActivityInfixToPostfix","Syntax error in the activitySteps? equation = "+equation);
        }
        //Retriving preferences, to check if the item was bought and deactivate the ads
        boolean flagPurchase = this.getSharedPreferences(getString(R.string.preference_file_name),Context.MODE_PRIVATE).getBoolean(getString(R.string.purchase_status), false);
        Log.d("MainAct/onCreate","preference flag = "+flagPurchase);
        // Managing fragment
        // Check that the activity is using the layout version with the fragment_container FrameLayout
        if (findViewById(R.id.ads_banner) != null) {

            /* However, if we're being restored from a previous state,
             * then we don't need to do anything and should return or else
             * we could end up with overlapping fragments.*/
            if (savedInstanceState != null) {
                return;
            }

            if (flagPurchase == false){

                // Create a new Fragment to be placed in the activity layout
                BannerAdsFragment firstFragment = new BannerAdsFragment();

                /* In case this activity was started with special instructions from an
                 * Intent, pass the Intent's extras to the fragment as arguments*/
                firstFragment.setArguments(getIntent().getExtras());

                // Add the fragment to the 'fragment_container' FrameLayout
                getSupportFragmentManager().beginTransaction().add(R.id.ads_banner, firstFragment).commit();

            }

        }
    }

    //I don't need the oncreateoptionsmenu, because the actionbar is empty!
    /*public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar_postfix_procedure, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
